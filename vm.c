/******************************************************************************
** Student name: 	...
** Student number: 	...
** Course: 			Advanced Programming Techniques - S1 2018
******************************************************************************/

#include "vm.h"

int main(int argc, char ** argv)
{
    

    VmSystem *system=NULL;
    
    MenuFunction func;
    MenuItem *menu=NULL;
    
    if(argc != 3){
    	printf("\nInvalid arguments.");
    	return EXIT_FAILURE;
	}
    
    system=(VmSystem *)malloc(sizeof(VmSystem));
    
    
    if(system==NULL){
    	printf("\nError allocating memory to VmSystem");
    	return EXIT_FAILURE;
	}
    if(!systemInit(system)){
    	return EXIT_FAILURE;
	}
	
    if(!loadData(system,argv[1],argv[2])){
    	printf("\nError loading data");
    	return EXIT_FAILURE;
	}
	
	menu=(MenuItem *)malloc(MENU_LENGTH * (sizeof(MenuItem)));
	if(menu==NULL){
		printf("\nError allocating memory to MenuItem");
		return EXIT_FAILURE;
	}
    initMenu(menu);
	
    while(TRUE){
    	displayMenu(menu);
    	printf("\nPlease enter your choice (1-9)");
    	func=getMenuChoice(menu);
    	if(func==NULL){
    		continue;
		}
		
    	func(system);
    	if(func==abortProgram || func==saveAndExit){
			break;
		}
    	
	}
	free(menu);
	return EXIT_SUCCESS;	
}

void displayMenu(MenuItem *menu){
	printf("\nMain Menu");
    	printf("\n-------------------");
    	printf("\n1. %s",menu[DISPLAY].text);
    	printf("\n2. %s",menu[PURCHASE].text);
    	printf("\n3. %s",menu[SAVE].text);
    	printf("\n\nAdministrator-only Menu");
    	printf("\n-------------------");
    	printf("\n4. %s",menu[ADD].text);
    	printf("\n5. %s",menu[REMOVE].text);
    	printf("\n6. %s",menu[DISP_COINS].text);
    	printf("\n7. %s",menu[RESET_STOCK].text);
    	printf("\n8. %s",menu[RESET_COINS].text);
    	printf("\n9. %s",menu[ABORT].text);
}

    


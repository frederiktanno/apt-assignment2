/******************************************************************************
** Student name: 	...
** Student number: 	...
** Course: 			Advanced Programming Techniques - S1 2018
******************************************************************************/

#include "vm_menu.h"

/**
 * vm_menu.c handles the initialisation and management of the menu array.
 **/

/**
 * In this function you need to initialise the array of menu items
 * according to the text to be displayed for the menu. This array is
 * an array of MenuItem with text and a pointer to the function
 * that will be called.
 **/
void initMenu(MenuItem * menu)
{ 
		int i=0;
		for(i=0;i<MENU_LENGTH;i++){
			switch(i){
				case 0:
					strcpy(menu[i].text,"Display Items");
					
					break;
				case 1:
					strcpy(menu[i].text,"Purchase Items");
    				
    				break;
	    		case 2:
	    			strcpy(menu[i].text,"Save and Exit");
    				
	    			break;
	    		case 3:
	    			strcpy(menu[i].text,"Add Items");
    				
	    			break;
	    		case 4:
	    			strcpy(menu[i].text,"Remove Items");
    				
	    			break;
	    		case 5:
	    			strcpy(menu[i].text,"Display Coins");
    				
	    			break;
	    		case 6:
	    			strcpy(menu[i].text,"Reset Stock");
    				
	    			break;
	    		case 7:
	    			strcpy(menu[i].text,"Reset Coins");
    				
	    			break;
	    		case 8:
	    			strcpy(menu[i].text,"Abort Program");
    					
					break;
	    			
	    		default:
	    			printf("\nInvalid Input. Please try again\n");
			}
		}
		return;
}


/**
 * Gets input from the user and returns a pointer to the MenuFunction
 * that defines how to perform the user's selection. NULL is returned
 * if an invalid option is entered.
 **/
MenuFunction getMenuChoice(MenuItem * menu)
{
	int i;
	char *cache=NULL;
    char *input=NULL;
    input=inputFunction();
    
    	if(input==NULL){
    		return NULL;
		}
    	i=strtol(input,&cache,DECIMAL);
	free(input);
	input=NULL;
    	switch(i){
    		case 1:
    			menu[i].function=displayItems;
    			return menu[i].function;
    			break;
    		case 2:
    			menu[i].function=purchaseItem;
    			return menu[i].function;
    			break;
    		case 3:
    			menu[i].function=saveAndExit;
			
    			return menu[i].function;
		
    			break;
    		case 4:
    			menu[i].function=addItem;
			
    			return menu[i].function;
    			break;
    		case 5:
    			menu[i].function=removeItem;
	
    			return menu[i].function;
    			break;
    		case 6:
    			menu[i].function=displayCoins;
    			return menu[i].function;
    			break;
    		case 7:
    			menu[i].function=resetStock;
    			return menu[i].function;
    			break;
    		case 8:
    			menu[i].function=resetCoins;
    			return menu[i].function;
    			break;
    		case 9:
    			menu[i].function=abortProgram;
			return menu[i].function;		  			
    		default:
    			printf("\nInvalid Input. Please try again\n");
    			return NULL;
		}
}

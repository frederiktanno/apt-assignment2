/******************************************************************************
** Student name: 	...
** Student number: 	...
** Course: 			Advanced Programming Techniques - S1 2018
******************************************************************************/

#include "vm_options.h"

/**
 * vm_options.c this is where you need to implement the system handling
 * functions (e.g., init, free, load, save) and the main options for
 * your program. You may however have some of the actual work done
 * in functions defined elsewhere.
 **/

/**
 * Initialise the system to a known safe state. Look at the structure
 * defined in vm_system.h.
 **/
Boolean systemInit(VmSystem * system)
{
	List *list=NULL;
	list=(List *)malloc(sizeof(List));
	if(list==NULL){
		printf("\nError allocating memory to List");
		return FALSE;
	}
	system->stockFileName=NULL;
	system->coinFileName=NULL;
	system->itemList=list;
	system->itemList->size=0;
	system->itemList->head=NULL;
    return TRUE;
}

/**
 * Free all memory that has been allocated. If you are struggling to
 * find all your memory leaks, compile your program with the -g flag
 * and run it through valgrind.
 **/
void systemFree(VmSystem * system)
{ 
	Node *node=system->itemList->head;
	Node *temp=NULL;
	while(node!=NULL){
		temp=node;
		node=node->next;
		free(temp->data);
		free(temp);
	}
	free(system->itemList);
	free(system);
	
}

/**
 * Loads the stock and coin data into the system. You will also need to assign
 * the char pointers to the stock and coin file names in the system here so
 * that the same files will be used for saving. A key part of this function is
 * validation. A substantial number of marks are allocated to this function.
 **/
Boolean loadData(
    VmSystem * system, const char * stockFileName, const char * coinsFileName)
{
	if(!loadStock(system,stockFileName)){
		printf("\nItems file not found");
		return FALSE;
	}
	/*if(!loadCoins(system,coinsFileName)==FALSE){
		printf("\nCoins file not found");
		return FALSE;
	}*/
    return TRUE;
}

/**
 * Loads the stock file data into the system.
 **/
Boolean loadStock(VmSystem * system, const char * fileName)
{
	FILE *items=NULL;
	
	items=fopen(fileName,"r");
	
	if(items==NULL){
		printf("\nItems file not found");
		fclose(items);
		return FALSE;
	}
	else{
		system->stockFileName=fileName;
		if(!addStock(system,items)){/* adds the contents of the stock file into the linked lists*/
			printf("\nError assigning memory to nodes");
			fclose(items);
			return FALSE;
		}
		fclose(items);
	}
	
	
	
    return TRUE;
}

/**
 * Loads the coin file data into the system.
 **/
Boolean loadCoins(VmSystem * system, const char * fileName)
{
	FILE *coins=NULL;
	
	coins=fopen(fileName,"r");
	if(coins==NULL){
		printf("\nCoins file not found");
		fclose(coins);
		return FALSE;
	}
	else{
		system->coinFileName=fileName;
		fclose(coins);
	}
	
    return TRUE;
}

/**
 * Saves all the stock back to the stock file.
 **/
Boolean saveStock(VmSystem * system)
{
	FILE *file;
	Node *node=NULL;
	file=fopen(system->stockFileName,"w");
	if(file==NULL){
		fclose(file);
		return FALSE;
	}
	/*file=fopen(system->stockFileName,"a");*/
	/*Stock *stock=NULL;*/
	
	node=system->itemList->head;
	
	while(node!=NULL){
		fprintf(file,"%s|%s|%s|%d.%d|%d \n",node->data->id, node->data->name,
		node->data->desc, node->data->price.dollars, node->data->price.cents,
		node->data->onHand);
		node=node->next;
	}
    return TRUE;
}

/**
 * Saves all the coins back to the coins file.
 **/
Boolean saveCoins(VmSystem * system)
{
    return FALSE;
}

/**
 * This option allows the user to display the items in the system.
 * This is the data loaded into the linked list in the requirement 2.
 **/
void displayItems(VmSystem * system)
{ 
	Node *current=NULL;
	
	if(system->itemList==NULL){
		printf("\nThere are no items available");
		return;
	}
	if(system->itemList->size==0){
		printf("\nThere are no items available");
		return;
	}
	current=system->itemList->head;
	printf("\n\nItem Menu \n\n");
	printf("ID	|Name		     |Available  | Price");
	printf("\n----------------------------------------------------\n");
	while(current!=NULL){
		/*break function was here*/
		printf("%s	|%-20s|%-11d| $ %d.%d\n",current->data->id,current->data->name,current->data->onHand,
		current->data->price.dollars,current->data->price.cents);
		current=current->next;
		
	}
	
}

/**
 * This option allows the user to purchase an item.
 * This function implements requirement 5 of the assignment specification.
 **/
void purchaseItem(VmSystem * system)
{ 
	char *cache=NULL;
	char *input=NULL;
	int price=0;
	int change=0;
	int amount=0;
	Stock *stock=NULL;
	printf("\nPlease enter the id of the item you wish to purchase:");
	input=inputFunction();
	if(strcmp(input,EMPTY_STRING)==0){
		errorMessage();
		return;
	}
	stock=getStock(system,input);
	if(stock==NULL){
		printf("\nItem not found");
		return;
	}
	price=(stock->price.dollars * DOLLAR_TO_CENT)+(stock->price.cents);
	printf("\nYou have selected %s. This will cost you $ %d.%d dollars",stock->name,stock->price.dollars,stock->price.cents);
	printf("\nPlease hand over the money - type in the value of each note / coin in cents." );
	printf("\nPress enter on a new and empty line to cancel this purchase:" );
	
	while(price>0){
		printf("You still need to give us: $ %d.%2d",(int)(price/100),(int)(price%100));
		input=inputFunction();
		if(input[0]=='\n'){
			printf("\nTransaction cancelled");
			return;
		}
		amount=(int)(strtol(input,&cache,DECIMAL));
		price-=amount;
			
		printf("\nAmount given: %d",amount);
	}
	if(price==0){
		stock->onHand--;
		printf("\nThank you. Here is your %s.",stock->name);
		printf("\nPlease come back soon");
		return;
	}
	else if(price<0){
		change=price*-1;
		stock->onHand--;
		printf("\nThank you. Here is your %s. Your change is $ %d.%2d",stock->name,(int) (change / 100), (int) (change % 100));
		printf("\nPlease come back soon");
		return;
	}
	free(input);
	return;
}

/**
 * You must save all data to the data files that were provided on the command
 * line when the program loaded up, display goodbye and free the system.
 * This function implements requirement 6 of the assignment specification.
 **/
void saveAndExit(VmSystem * system)
{
	
	if(!saveStock(system)){
		printf("\nError saving data, failure in save and exit");
	} 
	systemFree(system);
	printf("\nnew data saved successfully");
	printf("\nGood bye!\n\n");
    
}

/**
 * This option adds an item to the system. This function implements
 * requirement 7 of of assignment specification.
 **/
void addItem(VmSystem * system)
{ 
	char *id=(char *)malloc(sizeof (char)* (ID_LEN + NEW_LINE_SPACE));
	Node *node=NULL;
	Stock *stock=(Stock *)malloc(sizeof(Stock));
	Price *price=(Price *)malloc(sizeof(Price));
	
	node=system->itemList->head;
	while(node->next!=NULL){
		if(strcmp(node->data->id,node->next->data->id)<0){
			strcpy(id,node->next->data->id);/*get the last ID in the list*/
		}
		node=node->next;
		/*traverse until the last list*/
	}
	printf("\n%s",id);
	id=incrementId(id);
	printf("\nThis new meal item will have the new Id of: %s",id);
	strcpy(stock->id,id);
	if(!stockAddition(system,stock,price)){
		printf("\nError adding the new item");
		return;
	}
	free(id);
	id=NULL;
	free(price);
	price=NULL;
}

/**
 * Remove an item from the system, including free'ing its memory.
 * This function implements requirement 8 of the assignment specification.
 **/
void removeItem(VmSystem * system)
{ 
	char *input=NULL;
	Stock *stock=NULL;
	Node *node=NULL;
	Node *temp=NULL;

	printf("\nEnter the ID of the item to remove from the menu:");
	input=inputFunction();
	
	if(strcmp(input,EMPTY_STRING)==0){
		errorMessage();
		return;
	}
	stock=getStock(system,input);/*find the id which the user requested*/
	if(stock==NULL){
		printf("\nItem not found");
		return;
	}
	node=system->itemList->head;
	if(node!=NULL && strcmp(stock->id,system->itemList->head->data->id)==0){
		temp=system->itemList->head;
		system->itemList->head=system->itemList->head->next;
		/*temp=node->next;
		node->next=node->next->next;*/
	}
	else{
		while(node!=NULL){
			if(strcmp(stock->id,node->next->data->id)==0){
				temp=node->next;
				node->next=node->next->next;
				break;
			}
			node=node->next;
		}
	}
	
	printf("\n%s %s %s has been removed from the system",temp->data->id,temp->data->name,
	temp->data->desc);
	free(temp->data);
	free(temp);
	free(input);
	system->itemList->size--;
	return;
}

/**
 * This option will require you to display the coins from lowest to highest
 * value and the counts of coins should be correctly aligned.
 * This function implements part 4 of requirement 18 in the assignment
 * specifications.
 **/
void displayCoins(VmSystem * system)
{ printf("not implemented yet");}

/**
 * This option will require you to iterate over every stock in the
 * list and set its onHand count to the default value specified in
 * the startup code.
 * This function implements requirement 9 of the assignment specification.
 **/
void resetStock(VmSystem * system)
{ 
	Node *node=NULL;
	node=system->itemList->head;
	while(node!=NULL){
		node->data->onHand=DEFAULT_STOCK_LEVEL;
		node=node->next;
	}
	printf("\nAll stock has been reset to the default level of %d",DEFAULT_STOCK_LEVEL);
}

/**
 * This option will require you to iterate over every coin in the coin
 * list and set its 'count' to the default value specified in the
 * startup code.
 * This requirement implements part 3 of requirement 18 in the
 * assignment specifications.
 **/
void resetCoins(VmSystem * system)
{
	printf("not implemented yet");
}

/**
 * This option will require you to display goodbye and free the system.
 * This function implements requirement 10 of the assignment specification.
 **/
void abortProgram(VmSystem * system)
{
	
	systemFree(system);
	printf("Good bye! \n\n");
 }

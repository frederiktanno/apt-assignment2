/******************************************************************************
** Student name: 	...
** Student number: 	...
** Course: 			Advanced Programming Techniques - S1 2018
******************************************************************************/

#include "vm_stock.h"

/**
 * vm_stock.c this is the file where you will implement the
 * interface functions for managing the stock list.
 **/
Boolean addStock(VmSystem *system, FILE *itemfile){
	char *buffer=(char *)malloc(sizeof(char) * (ID_LEN + NAME_LEN + DESC_LEN + sizeof(Price) + STOCK_SIZE));
	char *cache=NULL;
	char *priceTemp=NULL;/*temporarily stores price value*/
	char *token=NULL;
	Price *price;
	Stock *stock = NULL;
	char *fetch=NULL;	
	
	priceTemp=(char *)malloc(sizeof(char) * (NAME_LEN));
	
	while((fetch=fgets(buffer,FILE_BUFFER,itemfile))!=NULL){/*read the contents of the stock file*/
		stock=(Stock *)malloc(sizeof(Stock));
		price=(Price *)malloc(sizeof(Price));
		
		token=strtok(buffer,STOCK_DELIM);
		strcpy(stock->id,token);
		stock->id[ID_LEN]='\0';
		token=strtok(NULL,STOCK_DELIM);
		strcpy(stock->name,token);
		stock->name[NAME_LEN]='\0';
		
		token=strtok(NULL,STOCK_DELIM);
		strcpy(stock->desc,token);
		stock->desc[DESC_LEN]='\0';
		
		token=strtok(NULL,STOCK_DELIM);
		strcpy(priceTemp,token);
		
		/*addPrice was here, needed to be moved cause strtok buffers previous tokenised characters*/

		token=strtok(NULL,STOCK_DELIM);
		stock->onHand=(unsigned int)strtol(token,&cache,DECIMAL);
		
		stock->price=addPrice(stock,priceTemp,price);
				
		if(!addNode(system,stock)){
			printf("\nError in adding a new node");
			return FALSE;
		}
		free(price);
		price=NULL;
	}
	free(buffer);
	buffer=NULL;
	free(priceTemp);
	priceTemp=NULL;
	return TRUE;
}

Price addPrice(Stock *stock, char *priceTemp, Price *price){
	char *token=NULL;
	char *cache=NULL;
	token=strtok(priceTemp,DELIM_CENT);
	price->dollars=(unsigned int)strtol(token,&cache,DECIMAL);
	token=strtok(NULL,DELIM_CENT);
	if(token!=NULL){

		price->cents=(unsigned int)strtol(token,&cache,DECIMAL);
	}
	else{
		price->cents=0;
	}
	return *price;
}

Boolean addNode(VmSystem *system, Stock *stock){
	Node *current=NULL;
	Node *prev=NULL;
	Node *ptr;
	ptr=(Node *)malloc(sizeof(Node));
	ptr->next=NULL;
	current=system->itemList->head;
	if(system->itemList->head==NULL){/*if head is empty*/
		ptr->next=NULL;
		system->itemList->head=ptr;
		ptr->data=stock;
		system->itemList->size++;
		return TRUE;
	}
	
	if(strcmp(stock->name, current->data->name)<0){/*if new item < head*/
		ptr->next=system->itemList->head;
		system->itemList->head=ptr;
		system->itemList->head->data=stock;
		system->itemList->size++;
	}
	else{
		
		while(current!=NULL){
			if(strcmp(stock->name, current->data->name)>0){/* if new item > current*/
				prev=current;
				current=current->next;
				continue;
			}
			else{
				prev->next=ptr;
				ptr->data=stock;
				ptr->next=current;
				system->itemList->size++;
				return TRUE;
			}
			
		}
		/* adds item to the end of the list*/
		prev->next=ptr;
		ptr->data=stock;
		system->itemList->size++;
	}
	
	return TRUE;	
}

Stock *getStock(VmSystem *system,char *choice){
	Stock *stock=NULL;
	Node *node=NULL;
	node=system->itemList->head;
	while(node!=NULL){
		if(strcmp(choice,node->data->id)==0){
			stock=node->data;
			return stock;
		}
		node=node->next;
	}
	return stock;
}

Boolean stockAddition(VmSystem *system, Stock *stock, Price *price){
	char *input=NULL;
	char *eptr;
	char *token=NULL;
	while(TRUE){
		printf("\nEnter the new item name: ");
		input=itemInput(NAME_LEN);
		if(input==NULL){
			printf("\nError assigning memory to input");
			break;
		}
		strcpy(stock->name,input);
		printf("\nEnter the new item description: ");
		input=itemInput(DESC_LEN);
		if(input==NULL){
			printf("\nError assigning memory to input");
			break;
		}
		strcpy(stock->desc,input);
		printf("\nEnter the price for this item: ");
		input=itemInput(ID_LEN);
		if(input==NULL){
			printf("\nError assigning memory to input");
			break;
		}
		token=strtok(input,DELIM_CENT);
		price->dollars=(int)strtol(token,&eptr,DECIMAL);
		if(token==NULL){
			printf("\nInvalid input. The currency format is DOLLARS.CENTS");
			continue;
		}
		token=strtok(NULL,DELIM_CENT);
		
		if(token!=NULL){
	
			price->cents=(unsigned int)strtol(token,&eptr,DECIMAL);
		}
		else{
			price->cents=0;
		}
		stock->price=(*price);
		stock->onHand=DEFAULT_STOCK_LEVEL;
			
		break;
	}
	if(!addNode(system,stock)){
		printf("\nError assigning memory to nodes");
		return FALSE;
	}
	printf("\nThis item: %s %s has been added to the menu",stock->name,stock->desc);
	free(input);
	return TRUE;
}

char * incrementId(char *id){
	char *eptr;
	char *token=NULL;
	char *temp=(char *)malloc(sizeof (char)* (ID_LEN + NEW_LINE_SPACE));
	int idnum;
	int iterator;
	size_t idlen,templen;
	idlen=strlen(id);
	token=strtok(id,ID_DELIM);/* remove the "I", resulting in 000x*/
	idnum=(int)strtol(token,&eptr,DECIMAL);/* convert into integer*/
	sprintf(id,"%d",idnum);
	idnum=(int)strtol(id,&eptr,DECIMAL);
	idnum++;/*increment Id*/
	sprintf(temp,"%d",idnum);/*convert to string*/
	templen=strlen(temp);
	strcpy(id,"I");/*adds an "I" to the beginning of the ID*/
	while(iterator<((idlen-templen))-1){
		/* leads leading zeroes until it matches the length of the ID subtracted by
			length of the new ID number (5)*/
		iterator++;
		strcat(id,"0");
	}
	strcat(id,temp);/*concatenates the ID number*/
	free(temp);
	return id;
}
/**
 * Some example functions:
 * create list, free list, create node, free node, insert node, etc...
 */

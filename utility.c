/******************************************************************************
** Student name: 	...
** Student number: 	...
** Course: 			Advanced Programming Techniques - S1 2018
******************************************************************************/

#include "utility.h"

/**
 * Function required to be used when clearing the buffer. It simply reads
 * each char from the buffer until the buffer is empty again. Please refer
 * to the materials on string and buffer handling in the course for more
 * information.
 **/
void clearBuffer(char input[INPUT_BUFFER]){
	int i=0;
	for(i=0;i<INPUT_BUFFER;i++){
		input[INPUT_BUFFER]=' ';
	}
}
	

void readRestOfLine(){
	int ch=0;
    while(ch = getc(stdin), ch != EOF && ch != '\n')
    { } /* Gobble each character. */

    /* Reset the error status of the stream. */
    clearerr(stdin);
}

void clearNewLine(char input[INPUT_BUFFER]){
	input[strcspn(input,"\n")]=0;
}

void errorMessage(){
	printf("\nInvalid command. Please try again\n");
}

char* inputFunction(void){		
	char *input=malloc(sizeof(char)*(INPUT_BUFFER + NEW_LINE_SPACE));
	if(input==NULL){
		printf("\nfailed to allocate memory for input storage");
		return NULL;
	}
	fgets(input,INPUT_BUFFER,stdin);
	if(strcmp(input,"\n")==0){
		return "\n";
	}
	fflush(stdin);
	/*for(i=0;input[INPUT_BUFFER];i++){
		if(input[i]==NEW_LINE){
			input[i]=NULL_SPACE_LINE;
		}
	}*/
	input[strcspn(input,"\n")]=0;
	return input;
}


char* itemInput(int buffer){/*input function for inserting item details*/

	char *input=malloc(sizeof(char)*(buffer + NEW_LINE_SPACE));
	buffer=buffer+NEW_LINE_SPACE;
	if(input==NULL){
		printf("\nfailed to allocate memory for input storage");
		return NULL;
	}
	fgets(input,buffer,stdin);
	fflush(stdin);
	/*for(i=0;input[INPUT_BUFFER];i++){
		if(input[i]==NEW_LINE){
			input[i]=NULL_SPACE_LINE;
		}
	}*/
	input[strcspn(input,"\n")]=0;
	return input;
}

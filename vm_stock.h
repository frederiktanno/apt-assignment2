/******************************************************************************
** Student name: 	...
** Student number: 	...
** Course: 			Advanced Programming Techniques - S1 2018
******************************************************************************/

#ifndef VM_STOCK_H
#define VM_STOCK_H

#include "vm_coin.h"
#include "vm_system.h"
#include "utility.h"

/**
 * The default stock level that all new stock should start with and the value
 * to use when restting stock.
 **/
#define DEFAULT_STOCK_LEVEL 20

#define STOCK_SIZE 100

#define STOCK_DELIM "|"

#define ID_DELIM "I"


Boolean addStock(VmSystem *system,FILE *itemfile);
Price addPrice(Stock *stock, char *priceTemp, Price *price);
Boolean addNode(VmSystem *system, Stock *stock);
Stock* getStock(VmSystem *system, char *choice);
Boolean stockAddition(VmSystem *system,Stock *stock, Price *price);

char * incrementId(char *id);

#endif

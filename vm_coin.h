/******************************************************************************
** Student name: 	...
** Student number: 	...
** Course: 			Advanced Programming Techniques - S1 2018
******************************************************************************/

#ifndef VM_COIN_H
#define VM_COIN_H

#include "vm_system.h"

/**
 * The default coin level used when resetting values.
 **/
#define DEFAULT_COIN_COUNT 20
#define CENT_5 5
#define CENT_10 10
#define CENT_20 20
#define CENT_50 50
#define DOLLAR_1 100
#define DOLLAR_2 200
#define DOLLAR_5 500
#define DOLLAR_10 1000

#define COIN_DELIM ","

#endif

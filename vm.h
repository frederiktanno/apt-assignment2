/******************************************************************************
** Student name: 	...
** Student number: 	...
** Course: 			Advanced Programming Techniques - S1 2018
******************************************************************************/

#ifndef VM_H
#define VM_H
#include "vm_system.h"
#include "vm_menu.h"
#include "utility.h"

void displayMenu(MenuItem *menu);

#endif

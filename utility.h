/******************************************************************************
** Student name: 	...
** Student number: 	...
** Course: 			Advanced Programming Techniques - S1 2018
******************************************************************************/

#ifndef UTILITY_H
#define UTILITY_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <assert.h>

typedef enum boolean
{
    FALSE = 0,
    TRUE
} Boolean;

#define NEW_LINE_SPACE 1
#define NEW_LINE '\n'
#define NULL_SPACE_LINE '\0'
#define NULL_SPACE 1
#define INPUT_BUFFER 30
#define FILE_BUFFER 255
#define DECIMAL 10
#define DOLLAR_TO_CENT 100
#define DELIM_CENT "."
/**
 * This is used to compensate for the extra character spaces taken up by
 * the '\n' and '\0' when input is read through fgets().
 **/
#define EXTRA_SPACES (NEW_LINE_SPACE + NULL_SPACE)

#define EMPTY_STRING ""

/**
 * Call this function whenever you detect buffer overflow.
 **/
char *input;
void readRestOfLine();

void errorMessage();

char* inputFunction(void);

char *itemInput(int buffer);

void clearNewLine(char input[INPUT_BUFFER]);

void clearBuffer(char input[INPUT_BUFFER]);


#endif
